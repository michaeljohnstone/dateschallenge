﻿using System;
using System.Collections.Generic;

namespace DatesChallenge
{
    public class BusinessDayCounter
    {
        public int WeekdaysBetweenTwoDates(DateTime firstDate, DateTime secondDate)
        {
            return BusinessDaysBetweenTwoDates(firstDate, secondDate, new List<DateTime>());
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<DateTime> publicHolidays)
        {
            if (secondDate <= firstDate)
                return 0;

            var totalDays = 0;
            foreach (var day in DaysBetween(firstDate, secondDate))
            {
                if (!DayIsWeekend(day) && day != firstDate && day != secondDate && !publicHolidays.Contains(day))
                {
                    totalDays++;
                }
            }

            return totalDays;
        }

        private static bool DayIsWeekend(DateTime day)
        {
            return day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday;
        }

        private  IEnumerable<DateTime> DaysBetween(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }
    }
}
