﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    //Rule for days that occur on a XXth day of the month
    //E.g. Queen's birthday on second Monday in June every year
    public class DayOccurenceRule : PublicHolidayRule
    {
        private int _occurence;
        private DayOfWeek _dayOfWeek;
        private Month _month;

        public DayOccurenceRule(int occurence, DayOfWeek dayOfWeek, Month month)
        {
            _occurence = occurence;
            _dayOfWeek = dayOfWeek;
            _month = month;
        }

        public override DateTime GetDate(int year)
        {
            DateTime startOfXthWeek = GetStartOfXthWeek(year, _occurence, (int)_month);
            int daysToAdd;

            if ((int)startOfXthWeek.DayOfWeek < (int)_dayOfWeek)
            {
                //if day of week we are looking for has not happened this week
                daysToAdd = (int)_dayOfWeek - (int)startOfXthWeek.DayOfWeek;
            }
            else if ((int)startOfXthWeek.DayOfWeek == (int)_dayOfWeek)
            {
                //startOfXthWeek will always be the xth occurence already, so no more days to add
                daysToAdd = 0;
            }
            else
            {
                //if the day we are looking for has already occured this week
                daysToAdd = 7 - ((int)startOfXthWeek.DayOfWeek - (int)_dayOfWeek);
            }

            return startOfXthWeek.AddDays(daysToAdd);
        }

        //E.g. if you are after the start of the second week of June 2021, this will be return 8th June
        private DateTime GetStartOfXthWeek(int year, int occurence, int month)
        {
            var startWeek = occurence - 1;
            var something = new DateTime(year, month, (startWeek * 7) + 1);
            return something;
        }
    }
}
