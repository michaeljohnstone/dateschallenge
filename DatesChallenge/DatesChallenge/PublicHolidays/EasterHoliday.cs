﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    public enum EasterHoliday
    {
        GoodFriday,
        EasterSaturday,
        EasterSunday,
        EasterMonday
    }
}
