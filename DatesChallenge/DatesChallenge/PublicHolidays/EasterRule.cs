﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    public class EasterRule : PublicHolidayRule
    {
        private EasterHoliday _easterHoliday;
        public EasterRule(EasterHoliday easterHoliday)
        {
            _easterHoliday = easterHoliday;
        }
        public override DateTime GetDate(int year)
        {
            var easterSunday = EasterSunday(year);

            return _easterHoliday switch
            {
                EasterHoliday.GoodFriday => easterSunday.AddDays(-2),
                EasterHoliday.EasterSaturday => easterSunday.AddDays(-1),
                EasterHoliday.EasterMonday => easterSunday.AddDays(1),
                _ => easterSunday,
            };
        }

        //Based on https://www.codeproject.com/Articles/10860/Calculating-Christian-Holidays
        private DateTime EasterSunday(int year)
        {
            int g = year % 19;
            int c = year / 100;
            int h = h = (c - (int)(c / 4) - (int)((8 * c + 13) / 25)
                                                + 19 * g + 15) % 30;
            int i = h - (int)(h / 28) * (1 - (int)(h / 28) *
                        (int)(29 / (h + 1)) * (int)((21 - g) / 11));

            int day = i - (year + year / 4 +
              i + 2 - c + c / 4) % 7 + 28;
            int month = 3;

            if (day > 31)
            {
                month++;
                day -= 31;
            }

            return new DateTime(year, month, day);
        }
    }
}
