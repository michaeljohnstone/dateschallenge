﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    public class GazettedPublicHolidayRule : PublicHolidayRule
    {
        private Month _month;
        private int _day;
       
        public GazettedPublicHolidayRule(Month month, int day)
        {
            _month = month;
            _day = day;
        }

        public override DateTime GetDate(int year)
        {
            var actualDate = new DateTime(year, (int)_month, _day);
            DateTime publicHoliday = actualDate;
            if (actualDate.DayOfWeek == DayOfWeek.Saturday)
                publicHoliday = actualDate.AddDays(2);
            if (actualDate.DayOfWeek == DayOfWeek.Sunday)
                publicHoliday = actualDate.AddDays(1);
            
            //handle Christmas/BoxingDay logic 
            var christmas = new DateTime(year, 12, 25);
            var boxingDay = new DateTime(year, 12, 26);
            //if Christmas is a Saturday, then Christmas holiday is on Monday, and Boxing day holiday is on Tuesday
            if (actualDate == boxingDay && christmas.DayOfWeek == DayOfWeek.Saturday)
                publicHoliday = actualDate.AddDays(2);
            //if Christmas is a Sunday, then Boxing day will remain as Monday, and Christmas becomes the Tuesday
            if (actualDate == christmas && christmas.DayOfWeek == DayOfWeek.Sunday)
                publicHoliday = actualDate.AddDays(2);

            return publicHoliday;
        }
    }
}
