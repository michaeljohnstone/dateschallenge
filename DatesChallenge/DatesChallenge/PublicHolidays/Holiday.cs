﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    public enum Holiday
    {
        NewYearsDay,
        AustraliaDay,
        GoodFriday,
        EasterSaturday,
        EasterSunday,
        EasterMonday,
        AnzacDay,
        QueensBirthday,
        LabourDay,
        Christmas,
        BoxingDay
    }
}
