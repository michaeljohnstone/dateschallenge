﻿using DatesChallenge.PublicHolidays;
using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    public class NswPublicHoliday
    {
        private PublicHolidayRule _rule;
        public NswPublicHoliday(Holiday holiday)
        {
            _rule = holiday switch
            {
                Holiday.NewYearsDay => new GazettedPublicHolidayRule(Month.January, 1),
                Holiday.AustraliaDay => new GazettedPublicHolidayRule(Month.January, 25),
                Holiday.GoodFriday => new EasterRule(EasterHoliday.GoodFriday),
                Holiday.EasterSaturday => new EasterRule(EasterHoliday.EasterSaturday),
                Holiday.EasterSunday => new EasterRule(EasterHoliday.EasterSunday),
                Holiday.EasterMonday => new EasterRule(EasterHoliday.EasterMonday),
                Holiday.AnzacDay => new StaticDateRule(Month.April, 25),
                Holiday.QueensBirthday => new DayOccurenceRule(2, DayOfWeek.Monday, Month.June),
                Holiday.LabourDay => new DayOccurenceRule(1, DayOfWeek.Monday, Month.October),
                Holiday.Christmas => new GazettedPublicHolidayRule(Month.December, 25),
                Holiday.BoxingDay => new GazettedPublicHolidayRule(Month.December, 26),
                _ => throw new ArgumentException("Not a valid public holiday."),
            };
        }

        public DateTime GetDate(int year)
        {
            return _rule.GetDate(year);
        }
    }
}
