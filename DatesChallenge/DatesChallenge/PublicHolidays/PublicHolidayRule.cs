﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    public abstract class PublicHolidayRule
    {
        public PublicHolidayRule()
        {
        }

        public abstract DateTime GetDate(int year); 
    }
}
