﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatesChallenge.PublicHolidays
{
    public class StaticDateRule : PublicHolidayRule
    {
        private Month _month;
        private int _day;
        public StaticDateRule(Month month, int day)
        {
            _month = month;
            _day = day;
        }
            
        public override DateTime GetDate(int year)
        {
            return new DateTime(year, (int)_month, _day);
        }
    }
}
