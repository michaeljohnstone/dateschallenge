using DatesChallenge;
using System;
using System.Collections.Generic;
using Xunit;

namespace DatesChallengeTests
{
    public class BusinessDayCounterTests
    {
        private readonly BusinessDayCounter _counter;
        private readonly List<DateTime> _commonPublicHolidays = new List<DateTime>()
        {
            new DateTime(2013, 12, 25),
            new DateTime(2013, 12, 26),
            new DateTime(2014, 1, 1)
        };
        public BusinessDayCounterTests()
        {
            _counter = new BusinessDayCounter();
        }

        [Theory]
        [InlineData("2013-10-7", "2013-10-7", 0)]
        [InlineData("2013-10-7", "2013-10-9", 1)]
        [InlineData("2013-10-5", "2013-10-14", 5)]
        [InlineData("2013-10-7", "2014-1-1", 61)]
        public void WeekdaysBetweenTwoDates_ReturnsExpectedValue(DateTime firstDate, DateTime secondDate, int expected)
        {
            var result = _counter.WeekdaysBetweenTwoDates(firstDate, secondDate);
            Assert.Equal(expected, result);
        }


        [Theory]
        [InlineData("2013-10-7", "2013-10-9", 1)]
        [InlineData("2013-12-24", "2013-12-27", 0)]
        [InlineData("2013-10-7", "2014-01-01", 59)]
        public void BusinessDaysBetweenTwoDates_ReturnsExpectedValue(DateTime firstDate, DateTime secondDate, int expected)
        {
            var result = _counter.BusinessDaysBetweenTwoDates(firstDate, secondDate, _commonPublicHolidays);

            Assert.Equal(expected, result);
        }
    }
}
