﻿using DatesChallenge;
using DatesChallenge.PublicHolidays;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DatesChallengeTests
{
    public class DayOccurenceRuleTests
    {
        [Fact]
        public void QueensBirthday2021()
        {
            var rule = new DayOccurenceRule(2, DayOfWeek.Monday, Month.June);
            var result = rule.GetDate(2021);
            Assert.Equal(new DateTime(2021, 6, 14), result);
        }

        [Fact]
        public void SecondThursdayOfApril2021()
        {
            var rule = new DayOccurenceRule(2, DayOfWeek.Thursday, Month.April);
            var result = rule.GetDate(2021);
            Assert.Equal(new DateTime(2021, 4, 8), result);
        }
    }
}
