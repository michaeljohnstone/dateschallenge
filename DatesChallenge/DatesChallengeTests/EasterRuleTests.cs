﻿using DatesChallenge;
using DatesChallenge.PublicHolidays;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DatesChallengeTests
{
    public class EasterRuleTests
    {
        [Fact]
        public void GoodFriday2021()
        {
            var rule = new EasterRule(EasterHoliday.GoodFriday);
            var result = rule.GetDate(2021);

            Assert.Equal(new DateTime(2021, 4, 2), result);
        }

        [Fact]
        public void EasterSaturday2021()
        {
            var rule = new EasterRule(EasterHoliday.EasterSaturday);
            var result = rule.GetDate(2021);

            Assert.Equal(new DateTime(2021, 4, 3), result);
        }

        [Fact]
        public void EasterSunday2021()
        {
            var rule = new EasterRule(EasterHoliday.EasterSunday);
            var result = rule.GetDate(2021);

            Assert.Equal(new DateTime(2021, 4, 4), result);
        }

        [Fact]
        public void EasterMonday2021()
        {
            var rule = new EasterRule(EasterHoliday.EasterMonday);
            var result = rule.GetDate(2021);

            Assert.Equal(new DateTime(2021, 4, 5), result);
        }

        [Fact]
        public void EasterSunday1950()
        {
            var rule = new EasterRule(EasterHoliday.EasterSunday);
            var result = rule.GetDate(1950);

            Assert.Equal(new DateTime(1950, 4, 9), result);
        }
    }
}
