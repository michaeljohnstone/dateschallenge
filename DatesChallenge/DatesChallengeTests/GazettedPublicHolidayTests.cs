﻿using DatesChallenge.PublicHolidays;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DatesChallengeTests
{
    public class GazettedPublicHolidayTests
    {
        [Fact]
        public void NewYearsDay2022()
        {
            var rule = new GazettedPublicHolidayRule(DatesChallenge.Month.January, 1);

            var result = rule.GetDate(2022);
            //New Years Day 2022 is on a Saturday, so result should be 2 days after
            Assert.Equal(new DateTime(2022, 1, 3), result);
        }

        [Fact]
        public void NewYearsDay2023()
        {
            var rule = new GazettedPublicHolidayRule(DatesChallenge.Month.January, 1);

            var result = rule.GetDate(2023);
            //New Years Day 2023 is on a Sunday
            Assert.Equal(new DateTime(2023, 1, 2), result);
        }

        //when christmas is on a saturday, boxing day is on a sunday, then the boxing day public holiday jumps over the chrismtas gazetted day
        [Fact]
        public void BoxingDay2021()
        {
            var rule = new GazettedPublicHolidayRule(DatesChallenge.Month.December, 26);

            var result = rule.GetDate(2021);

            Assert.Equal(new DateTime(2021, 12, 28), result);
        }

        [Fact]
        public void BoxingDay2022()
        {
            var rule = new GazettedPublicHolidayRule(DatesChallenge.Month.December, 26);

            var result = rule.GetDate(2022);

            Assert.Equal(new DateTime(2022, 12, 26), result);
        }

        [Fact]
        public void ChristmasDay2022()
        {
            var rule = new GazettedPublicHolidayRule(DatesChallenge.Month.December, 25);

            var result = rule.GetDate(2022);

            Assert.Equal(new DateTime(2022, 12, 27), result);
        }
    }
}
