﻿using DatesChallenge;
using DatesChallenge.PublicHolidays;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DatesChallengeTests
{
    public class StaticDayRuleTests
    {
        [Fact]
        public void AnzacDay()
        {
            var rule = new StaticDateRule(Month.April, 25);
            var result = rule.GetDate(2021);
            Assert.Equal(new DateTime(2021, 4, 25), result);
        }
    }
}
