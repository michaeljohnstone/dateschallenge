# DesignCrowd Technical Challenge

## My process
I used TDD to complete the first two tasks. My process followed the Red-Green-Refactor methodology.

- I first started by writing a failing test.
- I then wrote the logic to solve the test.
- Every few tests I would step back and look for opportunities to refactor the logic. I would look for opportunities where code was duplicated, or where logic could be simplified.


Noticed that the WeekdaysBetweenTwoDates is the same logic as BusinessDaysBetweenTwoDates, just with an empty public holiday list

To complete the third task took a bit of experimentation on how to handle the different rules for different types of public holidays. I ended up settling by having a base Abstract Class which defined a GetDate(int year) method. 

SubClasses could implement this Class, and provide an implementation for the GetDate method which would return the public holiday for the specified year for that rule.

## Rules identified
- StaticDateRule - days that are the same date no matter what day of the year it falls on. E.g. Anzac Day

- GazettedPublicHolidayRule - if the day is a weekday, then that is the public holiday. But if the day is on a weekend, then the holiday is on the next available working day.

- DayOccurenceRule - days that fall on the Xth of ZZ day on any particular month. E.g. Queens birthday is the 2nd Monday of June.

- Easter - this rule is complicated dates change drastically each year. I used this guide https://www.codeproject.com/Articles/10860/Calculating-Christian-Holidays to determine the date for Easter Sunday, and then calculated other dates from this date.

After I identified the rules and the code structure, I then created unit tests that covered the logic for each rule.

After completing the logic to determine public holidays, I then modified the BusinessDayCounter function to accept a list of public holiday rules. This work is on the BusinessDayCounterShouldAcceptListOfPublicHolidayRules so that it can be viewed separately from the Main branch which has the base solution for Task One and Task Two.

# How to run tests
- Clone the solution 
- Ensure .NET Core 3.1 SDK is installed on your machine
- Navigate in a cmd prompt to DatesChallenge\DatesChallengeTests folder
- Run:
```
dotnet test
```